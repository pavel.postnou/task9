const express = require("express");
const roomController = require("../controllers/roomController.js");
const roomRouter = express.Router();

 
roomRouter.post("/create", roomController.authenticateJWT, roomController.checkRole, roomController.postRoom);
roomRouter.delete("/:id", roomController.authenticateJWT, roomController.checkRole, roomController.deleteRoom);
roomRouter.put("/:id", roomController.authenticateJWT, roomController.checkRole, roomController.updateRoom);
roomRouter.get("/get",  roomController.getAdminRooms);
roomRouter.get("/getRoom",roomController.authenticateJWT,roomController.getRoomById);

 
module.exports = roomRouter;