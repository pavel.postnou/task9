const express = require("express");
const authController = require("../controllers/authController.js");
const authRouter = express.Router();
const roomRouter = require ("../routes/roomRouter.js")
authRouter.post("/auth", authController.authorization, roomRouter);

module.exports = authRouter;
