const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength:3,
        maxlength:20,
        default:"noname"
    },
    id: {
        type: Number,
        required: true,
        min: 1,
        max:100,
        default:1
    },
    role: {
        type: String,
        default:"user"
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 10,
        default: "noPas"
    },
    age: {
        type:Number,
        required:true,
        min:1,
        max:120,
        default:1
    },
    rooms : [{
        type: mongoose.Schema.Types.ObjectId, ref:"Room",autopopulate: true
    }]

},
{versionKey: false }
);
userScheme.plugin(require('mongoose-autopopulate'));
const User = mongoose.model("User", userScheme);
module.exports = User;