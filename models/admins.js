const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 20,
        default: "unknown"
    },
    id: {
        type: Number,
        required: true,
        min: 1,
        max: 100,
        default: 1
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 10,
        default: "noPasdddd"
    },
    role:{
        type: String,
        default:"admin"
    },
    rooms : [{
        type: mongoose.Schema.Types.ObjectId, ref:"Room",
        autopopulate: true
    }]

},
    { versionKey: false }
);
adminScheme.plugin(require('mongoose-autopopulate'));
const Admin = mongoose.model("Admin", adminScheme);
module.exports = Admin;