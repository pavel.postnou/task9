const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roomScheme = new Schema({
    number: {
        type: Number,
        required: true,
        min: 1,
        max: 100,
        default: 1
    },
    users: [{
        type: mongoose.Schema.Types.ObjectId, ref:"User",
    }],
    admin: {
        type: mongoose.Schema.Types.ObjectId, ref:"Admin",
    }

},
    { versionKey: false }
);
const Room = mongoose.model("Room", roomScheme);
module.exports = Room;