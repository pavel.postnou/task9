const jwt = require ("jsonwebtoken")
const keys = require("../config/keys.js");
const Admin = require("../models/admins.js");
const Room = require("../models/rooms.js");
const User = require("../models/users.js")


exports.authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, keys.jwt, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

exports.checkRole = function (request,response,next){
    if (request.user.role !== "admin")
    {
        return response.status(403).send("Access denied")
    }
    next()
};

/*Здесь присоединение пользователя к комнате сделано для проверки методов
Я так понимаю, что лучше присоединение писать отдельным методом. Но не делал этого для экономии времени*/

exports.postRoom =  async function (req, res) {
let roomF = await Room.findOne({number:req.body.number});
let roomAdmin = await Admin.findOne({_id:req.user._id});
if (roomF) res.send("The room already exist")
else {
    let room = await new Room({number:req.body.number});
    room.admin = req.user._id;
    await room.save();
    roomAdmin.rooms.push(room._id);
    await roomAdmin.save();
    const userRoom = await User.findById({_id:req.body._id})
    userRoom.rooms.push(room._id)
    await userRoom.save()
    room.users = userRoom._id
    await room.save();
    res.send ("New room succesfully created")
}
}

 exports.deleteRoom = async function (request, response) {
    let roomF = await Room.findOne({_id: request.params.id});
    let roomAdmin = await Admin.findOne({_id: request.user._id});
    
    if (roomF && roomAdmin._id.toString() == roomF.admin._id.toString()){
        Room.deleteOne({_id: request.params.id}, function(err, result) {
            if (err) return res.send(err);
        });
            for (let i = 0; i < roomAdmin.rooms.length; i++) {
            if (roomAdmin.rooms[i]._id.toString() == roomF._id.toString()) 
            roomAdmin.rooms.splice(roomAdmin.rooms.indexOf(i),1); 
            await roomAdmin.save()}
       
        response.send(`Комната удалена`);  
    }
    else {
        response.send(`!!!Комната вам не принадлежит`);
    }
}


exports.updateRoom = async function (request, response) {
    let roomF = await Room.findOne({_id: request.params.id});
    let roomAdmin = await Admin.findOne({_id: request.user._id});

    if (roomF && roomAdmin._id.toString() == roomF.admin._id.toString()){
        Room.findByIdAndUpdate({_id:request.params.id}, {number:22}, function(err, doc) {
            if (err) return res.send(err);
            response.send(`Комната изменена `+ doc);  
        });

    }
    else {
        response.send(`!!!Комната вам не принадлежит`);
    }
}


exports.getAdminRooms = async function (request, response) {

    let adminOf = await Admin.findById({_id:request.body._id});
    response.send(adminOf.rooms)
    
}

exports.getRoomById = async function (request, response) {
    if (request.user.role  !== "admin") {
        let userRoom = await User.findOne({rooms: request.user.rooms})
        let room = await Room.findById({_id:request.body._id})
        for (let i = 0; i < userRoom.rooms.length; i++) {
            if (userRoom.rooms[i]._id.toString() ==  room._id.toString()) { 
            response.send(room)
            }
        }
        response.send("Вы не являетесь участником данной комнаты")
    }
    else {
    let room = await Room.findById({_id:request.body._id});
    response.send(room)
    }
    
    
}


