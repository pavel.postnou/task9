const jwt = require ("jsonwebtoken")
const User = require("../models/users")
const Admin = require("../models/admins")
const keys = require("../config/keys.js");

exports.authorization = ("/",  async function  (request, response,next){
    
    const user = await User.findOne({name:request.body.name, password:request.body.password})
    const admin = await Admin.findOne({name:request.body.name, password:request.body.password})
    if (user) {

        const accessToken = jwt.sign({ name: user.name,  role: user.role, rooms:user.rooms}, keys.jwt,{expiresIn: 60*60});

        response.json({
            accessToken
            
        });
        next()
    } 
    else if (admin) {

        const accessToken = jwt.sign({ name: admin.name,  role: admin.role, _id:admin._id  }, keys.jwt,{expiresIn: 60*60});

        response.json({
            accessToken
        });
        next()
    } else {
        response.send('name or password incorrect');
    }
});

