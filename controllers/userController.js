const User = require("../models/users.js");

module.exports = {
    postUser: (request, response) => {

    User.create({ name: request.body.name, id: request.body.id, age: request.body.age, password: request.body.password }, function (err, doc) {

        if (err) return console.log(err);

        response.send("<h1>Данные внесены</h1><p>id = " + doc + "</p>");
    });

},

    putUser : (request, response) => {
    mongoose.set("useFindAndModify", false);
    User.findOneAndUpdate({ id: request.params.id }, { id: request.body.id, name: request.body.name, age: request.body.age }, { new: true }, function (err, user) {
        if (err) return console.log(err);

        response.send("<h1>Обновление пользователя с</h1><p>id = " + user);
    });
},

    getUser : (request, response) => {
    User.find({ id: request.params.id }, function (err, docs) {

        if (err) return console.log(err);

        response.send("<h1>Получение данных пользователя</h1><p>id=" + docs + "</p>");
    });

},

    deleteUser : (request, response) =>{ 
    User.findOneAndDelete({ id: request.params.id }, function (err, doc) {

        if (err) return console.log(err);

        response.send("<h1>Удаление пользователя</h1><p>id=" + doc + "</p>")
    });

},
}